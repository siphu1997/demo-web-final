-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: localhost
-- Thời gian đã tạo: Th7 01, 2020 lúc 08:52 PM
-- Phiên bản máy phục vụ: 10.4.11-MariaDB
-- Phiên bản PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `demo_sql_injection2`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `role`
--

CREATE TABLE `role` (
  `role_id` int(11) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `role`
--

INSERT INTO `role` (`role_id`, `level`) VALUES
(1, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `test`
--

CREATE TABLE `test` (
  `id` int(11) NOT NULL,
  `cot_a` int(11) NOT NULL,
  `cot_b` int(11) NOT NULL,
  `cot_c` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user`
--

CREATE TABLE `user` (
  `id` int(10) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `role_id` int(10) DEFAULT 1,
  `gender` tinyint(1) DEFAULT NULL,
  `salary` double DEFAULT NULL,
  `job_position` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `phone`, `address`, `password`, `display_name`, `role_id`, `gender`, `salary`, `job_position`) VALUES
(1, 'hsp1997', 'siphu1997@gmail.com', '8686868686', '324 Bach Dang', 'zxc123', 'Hồ Sĩ Phú', 2, 1, 990000000, 'CEO'),
(2, 'lhphuc1997', 'lhphuc1997@gmail.com', '0203282716', '12/2 Bui Dinh Tuy', 'zxc123', 'Lê Hồng Phúc', 1, 1, 17000500, 'CTO'),
(3, 'ntbdiem002', 'ntbdiem002@gmail.com', '0274628264', '123 Dien Bien Phu', 'zxc123', 'Diễm', 1, 1, 100, 'Thư ký'),
(4, 'ltdlinh001', 'ltdl001@gmail.com', '0282737373', '111 Hoa Cuc', 'zxc123', 'Diệu Linh', 1, 1, 50000000, 'Kế toán trưởng'),
(5, 'levana', 'levana@gmail.com', '0727272736', '88 Hung Vuong', 'zxc123', 'Lê Văn A', 1, 1, 7000000, 'Nhân viên'),
(6, 'levanb', 'levanb@gmail.com', '0808087676', '99 Kinh Duong Vuong', 'zxc123', 'Lê Văn B', 1, 1, 9000000, 'Nhân viên');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`role_id`);

--
-- Chỉ mục cho bảng `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `role`
--
ALTER TABLE `role`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
