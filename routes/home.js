var express = require("express");
var router = express.Router();
const UserDB = require("../models/UserDB");
var ls = require("local-storage");
/* GET home page. */

router.use("/", function (req, res, next) {
  const data = ls.get("currentUser");
  if (data) {
    res.redirect("/manage");
  } else {
    next();
  }
});
router.get("/", async (req, res, next) => {
  const { msgErr } = req.query;
  res.render("home", {
    msgErr,
  });
  // try {
  //   const uDB = new UserDB();
  //   const data = await uDB.getAllUser();
  //   console.log(data);
  // } catch (error) {
  //   console.log(error);
  // }
});

module.exports = router;
