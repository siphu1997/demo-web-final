var express = require("express");
var router = express.Router();
var ls = require("local-storage");
/* GET home page. */
router.get("/", async function (req, res, next) {
  res.render("index", { title: "SQL Injection" });
});

router.get("/logout", async function (req, res) {
  let result = await ls.remove("currentUser");
  if (result) {
    res.json({ logout: true });
  } else {
    res.json({ logout: false });
  }
});

module.exports = router;
