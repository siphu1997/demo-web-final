var express = require("express");
var router = express.Router();
const UserDB = require("../models/UserDB");
const RoleDB = require("../models/RoleDB");
var ls = require("local-storage");

router.get("/", async (req, res) => {
  const uDB = new UserDB();
  const allUser = await uDB.getAllUser();

  const data = ls.get("currentUser");
  const userInfo = JSON.parse(data);
  if (data) {
    res.render("manage", {
      userInfo: {
        ...userInfo,
      },
      listUser: allUser,
    });
  } else {
    res.redirect("/home");
  }
});
router.post("/", async (req, res, next) => {
  const { username, pw } = req.body;
  if (!username || !pw) {
    return res.redirect("/home?msgErr=Vui lòng nhập dầy đủ tên và mật khẩu");
  }
  try {
    const uDB = new UserDB();
    const rDb = new RoleDB();
    const userInfo = await uDB.getUser(username, pw);
    const allUser = await uDB.getAllUser();
    const role = await rDb.getRole(userInfo.role_id);
    ls.set(
      "currentUser",
      JSON.stringify({
        ...userInfo,
        role: {
          level: role[0].level,
        },
      })
    );
    res.render("manage", {
      userInfo: {
        ...userInfo,
        role: {
          level: role[0].level,
        },
      },
      listUser: allUser,
    });
  } catch (error) {
    console.log(error);
    res.redirect("/home?msgErr=Tên đăng nhập hoặc mật khẩu không chính xác");
  }
});

module.exports = router;
