var express = require("express");
var router = express.Router();
const UserDB = require("../models/UserDB");
/* GET users listing. */
router.get("/", function (req, res, next) {
  res.send("respond with a resource");
});
router.put("/", async function (req, res, next) {
  try {
    const user = { ...req.body };
    const uDB = new UserDB();
    const newData = await uDB.updateUser(user);
    console.log(newData);
    res.json({ message: "Cập nhật thành công" });
  } catch (error) {
    console.log(error);
    res.json({ message: "Cập nhật thất bại", error: true });
  }
});

module.exports = router;
