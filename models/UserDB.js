const conn = require("./ConnectDB.js");

class UserDB {
  constructor() {}

  getUser(username, pw) {
    // let sql = `SELECT * FROM user WHERE username="${username}" and password="${pw}"`;
    let sql =
      "SELECT * FROM user WHERE username='" +
      username +
      "' and password='" +
      pw +
      "'";
    console.log("@SQL==: ", sql);
    // sql = `SELECT * FROM user WHERE username='' OR '1=1' and password='' OR '1=1' ; drop table test_delete; SELECT * FROM role WHERE '1=1' `;
    // console.log("SQL 2:", sql);
    // sql =
    //   " SELECT * FROM user WHERE username='' OR 1=1 ; drop table test_delete ; -- and password= ";
    // sql =
    //   " SELECT * FROM user WHERE username='' OR 1=1 UNION SELECT * FROM user ; -- and password= ";
    return new Promise((resolve, reject) => {
      conn.query(sql, function (err, result) {
        if (err) reject(err);
        console.log(result);
        if (result && result.length > 0) resolve(result[0]);
        reject("Not fault");
      });
    });
  }

  getUser(username, pw) {
    let sql =
      "SELECT * FROM user WHERE username='" +
      username +
      "' and password='" +
      pw +
      "'";
    console.log("@SQL==: ", sql);
    return new Promise((resolve, reject) => {
      conn.query(sql, function (err, result) {
        if (err) reject(err);
        console.log(result);
        if (result && result.length > 0) resolve(result[0]);
        reject("Not fault");
      });
    });
  }

  getUserPreventInjection(username, pw) {
    let sql = "SELECT * FROM user WHERE username=? and password=?";
    return new Promise((resolve, reject) => {
      conn.query(sql, [username, pw], function (err, result) {
        if (err) reject(err);
        console.log(result);
        if (result && result.length > 0) resolve(result[0]);
        reject("Not fault");
      });
    });
  }

  updateUser(user) {
    const {
      username,
      email,
      phone,
      address,
      display_name,
      gender,
      salary,
      job_position,
      id,
    } = user;
    const sql =
      "UPDATE user SET username='" +
      username +
      "' ,email='" +
      email +
      "' ,phone='" +
      phone +
      "' ,address='" +
      address +
      "' , display_name='" +
      display_name +
      "' ,gender='" +
      gender +
      "' ,salary='" +
      salary +
      "' ,job_position='" +
      job_position +
      "'  WHERE id='" +
      id +
      "'";
    return new Promise((resolve, reject) => {
      conn.query(sql, function (err, result) {
        if (err) reject(err);
        resolve(result);
      });
    });
  }

  getAllUser() {
    const sql =
      "SELECT `id`, `username`, `email`, `phone`, `address`, `display_name`, `gender`, `salary`, `job_position` FROM `user`";
    return new Promise((resolve, reject) => {
      conn.query(sql, function (err, result) {
        if (err) reject(err);
        resolve(result);
      });
    });
  }
}

module.exports = UserDB;
