class User {
  constructor(
    id,
    username,
    email,
    phone,
    address,
    password,
    display_name,
    role_id,
    gender,
    salary,
    job_position
  ) {
    this.id = id;
    this.username = username;
    this.email = email;
    this.phone = phone;
    this.address = address;
    this.password = password;
    this.display_name = display_name;
    this.role_id = role_id;
    this.gender = gender;
    this.salary = salary;
    this.job_position = job_position;
  }
}
