const mysql = require("mysql");

const conn = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "demo_sql_injection2",
  multipleStatements: true,
});

module.exports = conn;
