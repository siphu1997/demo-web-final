const conn = require("./ConnectDB.js");

class RoleDB {
  constructor() {}

  getRole(id) {
    const sql = `SELECT * FROM role WHERE role_id=${id}`;
    return new Promise((resolve, reject) => {
      conn.query(sql, function (err, result) {
        if (err) reject(err);
        resolve(result);
      });
    });
  }
}

module.exports = RoleDB;
